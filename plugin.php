<?php

use Facebook\FacebookRedirectLoginHelper as FacebookRedirectLoginHelper;
use Facebook\FacebookRequest as FacebookRequest;
use Facebook\GraphUser as GraphUser;
use Facebook\FacebookSession as FacebookSession;

class DatawrapperPlugin_SigninFacebook extends DatawrapperPlugin {

    public function init() {
        $plugin = $this;
        if (!$plugin->checkConfig()) {
            error_log('signin-facebook is not configured properly');
            return;
        }
        $config = $plugin->getConfig();
        FacebookSession::setDefaultApplication($config['app_id'], $config['app_secret']);

        // register plugin controller under /gallery/
        DatawrapperHooks::register(DatawrapperHooks::GET_PLUGIN_CONTROLLER, array($plugin, 'redirectEndpoint'));

        DatawrapperHooks::register(DatawrapperHooks::ALTERNATIVE_SIGNIN, function() use ($plugin) {
            $helper = $plugin->initFacebookHelper();
            global $app;
            return array(
                'icon' => 'fa fa-facebook',
                'label' => 'Facebook',
                'url' => $app->request()->getResourceUri() != '/signin/facebook' ? $helper->getLoginUrl() : ''
            );
        });

        $plugin->checkLogin();
    }

    public function checkConfig() {
        $config = $this->getConfig();
        return !empty($config['app_id']) && !empty($config['app_secret']);
    }

    public function initFacebookHelper($noAppId = false) {
        $redirect_url = 'http://' . $GLOBALS['dw_config']['domain'] . '/signin/facebook';
        $config = $this->getConfig();
        return new FacebookRedirectLoginHelper($redirect_url, $config['app_id'], $config['app_secret']);
    }

    /*
     * check if a user has been signed in properly
     */
    private function checkLogin() {
        if (!$this->checkConfig()) return;

        if (isset($_SESSION['signin/facebook/status']) && $_SESSION['signin/facebook/status']=='verified') {

            $fb_id = $_SESSION['signin/facebook/session/uid'];
            $fb_name = $_SESSION['signin/facebook/session/name'];
            $fb_url = $_SESSION['signin/facebook/session/link'];

            // check if we already have this facebook user in our database
            $user = UserQuery::create()->findOneByOAuthSignIn('facebook::' . $fb_id);
            if (!$user) {
                // if not we create a new one
                $user = new User();
                $user->setCreatedAt(time());
                $user->setOAuthSignIn('facebook::' . $fb_id);
                // we use the email field to store the facebookid
                $user->setEmail('');
                $user->setRole(UserPeer::ROLE_EDITOR); // activate user rigth away
                $user->setName($fb_name);
                $user->setSmProfile($fb_url);
                $user->save();
            }
            DatawrapperSession::login($user, true, true);
        }
    }

    public function redirectEndpoint($app) {
        if (!$this->checkConfig()) return;
        $plugin = $this;

        $app->get('/signin/facebook', function () use ($app, $plugin) {
            $req = $app->request();
            $helper = $this->initFacebookHelper();
            try {
                $session = $helper->getSessionFromRedirect();
                if ($session) {
                    $request = new FacebookRequest( $session, 'GET', '/me' );
                    $response = $request->execute();
                    // get response
                    $fb_user = $response->getGraphObject(GraphUser::className());
                    // Logged in.
                    $_SESSION['signin/facebook/status'] = 'verified';
                    $_SESSION['signin/facebook/session/uid'] = $fb_user->getId();
                    $_SESSION['signin/facebook/session/name'] = $fb_user->getName();
                    $_SESSION['signin/facebook/session/link'] = $fb_user->getLink();
                    try {
                        $app->redirect('/');
                    } catch (Slim_Exception_Stop $e) {}
                    return;
                }
            } catch(FacebookRequestException $ex) {
                // When Facebook returns an error
                error_log('Facebook returns an error:');
                error_log($ex);
            } catch(\Exception $ex) {
                // When validation fails or other local issues
                error_log('Validation fails or other local issues:');
                error_log($ex);
            }
            // not logged in
            session_destroy();
            $app->redirect('/');
        });
    }

}